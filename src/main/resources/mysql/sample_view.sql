drop view IF EXISTS `cart_items_and_total`;

CREATE
  ALGORITHM = UNDEFINED
  DEFINER = root@localhost
  SQL SECURITY DEFINER
  VIEW `cart_items_and_total` AS
select c.cart_id as cart_id, count(i.order_item_id) as items_count, c.total_price as total_price
from shopping_cart c
       left join order_items i on i.cart_id = c.cart_id;