package mk.ukim.finki.emt2019.ordermanagement.domain.commands;

import mk.ukim.finki.emt2019.ordermanagement.model.Account;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

/**
 * @author Riste Stojanov
 */
public class LinkToAccountCommand {

    @TargetAggregateIdentifier
    public Long cartId;

    public Account account;

    public LinkToAccountCommand() {
    }

    public LinkToAccountCommand(Long cartId, Account account) {
        this.cartId = cartId;
        this.account = account;
    }
}
