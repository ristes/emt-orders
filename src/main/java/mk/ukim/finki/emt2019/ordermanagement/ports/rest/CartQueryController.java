package mk.ukim.finki.emt2019.ordermanagement.ports.rest;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Riste Stojanov
 */
@RestController
public class CartQueryController {


    @GetMapping("/cart/{id}")
    public ShoppingCart getCart(@PathVariable Long id) {
        return null;
    }
}
