package mk.ukim.finki.emt2019.ordermanagement.domain.saga;

import mk.ukim.finki.emt2019.ordermanagement.domain.commands.ExpireCartCommand;
import mk.ukim.finki.emt2019.ordermanagement.domain.events.CartCreatedEvent;
import mk.ukim.finki.emt2019.ordermanagement.domain.events.CartExpiredEvent;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.scheduling.EventScheduler;
import org.axonframework.eventhandling.scheduling.ScheduleToken;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

/**
 * @author Riste Stojanov
 */

@Saga
@ProcessingGroup("cart_saga")
public class Expiry {

    public String cartId;
    @Autowired
    private CommandGateway commandGateway;
    @Autowired
    private EventScheduler eventScheduler;
    private ScheduleToken expiryScheduleToken;

    @StartSaga
    @SagaEventHandler(associationProperty = "cartId")
    public void on(CartCreatedEvent event) {
        this.cartId = event.cartId;

        this.expiryScheduleToken = eventScheduler.schedule(
                Instant.from(event.cartExpiry),
                new CartExpiredEvent(event.cartId)
        );
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "id")
    public void on(CartExpiredEvent event) {
        this.commandGateway.send(new ExpireCartCommand(event.cartId));
    }
}
