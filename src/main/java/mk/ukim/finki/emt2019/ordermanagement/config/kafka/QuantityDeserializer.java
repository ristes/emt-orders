package mk.ukim.finki.emt2019.ordermanagement.config.kafka;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;
import mk.ukim.finki.emt2019.ordermanagement.model.QuantityFactory;

import java.io.IOException;

/**
 * @author Riste Stojanov
 */
public class QuantityDeserializer extends JsonDeserializer<Quantity> {

    @Override
    public Quantity deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String valueAsString = jsonParser.getValueAsString();
        return QuantityFactory.parse(valueAsString);
    }
}
