package mk.ukim.finki.emt2019.ordermanagement.service;

import mk.ukim.finki.emt2019.ordermanagement.model.Quantity;

/**
 * @author Riste Stojanov
 */
public interface ProductQuantityValidator {

    boolean isProductQuantityValid(Quantity quantity);
}
