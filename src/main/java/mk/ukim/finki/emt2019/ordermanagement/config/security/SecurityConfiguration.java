package mk.ukim.finki.emt2019.ordermanagement.config.security;

import mk.ukim.finki.emt2019.ordermanagement.config.security.oauth.OAuthProvider;
import mk.ukim.finki.emt2019.ordermanagement.config.security.oauth.SignInFailureHandler;
import mk.ukim.finki.emt2019.ordermanagement.config.security.oauth.SignInSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.context.request.RequestContextListener;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Riste Stojanov
 */
@Configuration
@EnableOAuth2Client
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Qualifier("oauth2ClientContext")
    @Autowired
    OAuth2ClientContext oauth2ClientContext;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private UserDetailsService dummyUserDetails;

    @Autowired
    private JwtHelper jwtHelper;


    @Bean
    @Order(0)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);

        auth.userDetailsService(this.dummyUserDetails);


        auth.inMemoryAuthentication()
                .withUser("admin").password("admin123").roles("ADMIN")
                .and()
                .withUser("user").password("user123").roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
//                .requiresChannel()
//                .anyRequest()
//                .requiresSecure()
//                .and()
                .csrf().disable()

                .formLogin()
//                .loginPage("/login")
                .successHandler(new SignInSuccessHandler("LOGIN", jwtHelper))
                .failureHandler(new SignInFailureHandler())
                .and()
                .rememberMe().key("uniqueRembemberMeKey").tokenValiditySeconds(2592000)
                .and()
                .logout().clearAuthentication(true).invalidateHttpSession(true).deleteCookies("remember-me")
                .and()
                .authorizeRequests().antMatchers("/api/*", "/me", "/profile").hasRole("USER")
                .and()
                .authorizeRequests().antMatchers("/edit-profile-change-password").hasAuthority("CHANGE_PASSWORD_AUTHORITY")
                .and()
                .authorizeRequests().antMatchers("/*").permitAll();

        List<SecurityConfigurerAdapter> additionalOAuthProviders = fetchAdditionalOAuthConfigurations();
        for (SecurityConfigurerAdapter provider : additionalOAuthProviders) {
            http.apply(provider);
        }
        http.addFilterBefore(new JwtAuthorizationFilter(this.jwtHelper), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

    @Bean
    public FilterRegistrationBean oauth2ClientFilterRegistration(OAuth2ClientContextFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(filter);
        registration.setOrder(-100);
        return registration;
    }

    private List<SecurityConfigurerAdapter> fetchAdditionalOAuthConfigurations() {
        return applicationContext.getBeansWithAnnotation(OAuthProvider.class)
                .values().stream().map(i -> (SecurityConfigurerAdapter) i).collect(Collectors.toList());
    }

}
