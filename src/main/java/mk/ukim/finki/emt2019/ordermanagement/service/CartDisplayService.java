package mk.ukim.finki.emt2019.ordermanagement.service;

import mk.ukim.finki.emt2019.ordermanagement.service.view_model.CartTotalAndNumberOfItems;

/**
 * @author Riste Stojanov
 */
public interface CartDisplayService {

    CartTotalAndNumberOfItems getCartStatus(Long cartId);


}
