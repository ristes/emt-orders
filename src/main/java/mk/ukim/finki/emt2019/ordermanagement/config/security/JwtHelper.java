package mk.ukim.finki.emt2019.ordermanagement.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
@Component
public class JwtHelper {

    @Value("${app.jwt.secret}")
    private String secretKey;

    @Value("${app.jwt.validity}")
    private long validityInMilliseconds;

    Claims getClaims(String token) {
        return (Claims) Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token).getBody();
    }

    String getProviderId(String token) {
        return this.getClaims(token).getSubject();
    }

    public String generateToken(String providerId, String provider, Map authDetailsMap) {
        Claims claims = Jwts.claims().setSubject(providerId);
        claims.putAll(authDetailsMap);
        claims.put("provider", provider);

        Date now = new Date();
        Date validity = new Date(now.getTime() + this.validityInMilliseconds);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, this.secretKey)
                .compact();
    }
}
