package mk.ukim.finki.emt2019.ordermanagement.domain.events;

import mk.ukim.finki.emt2019.ordermanagement.model.Account;

/**
 * @author Riste Stojanov
 */
public class CartLinkedToAccountEvent {

    public Long cartId;

    public Account account;

    public CartLinkedToAccountEvent() {
    }

    public CartLinkedToAccountEvent(Long cartId, Account account) {
        this.cartId = cartId;
        this.account = account;
    }
}
