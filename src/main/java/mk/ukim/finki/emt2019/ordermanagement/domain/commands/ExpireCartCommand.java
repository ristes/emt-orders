package mk.ukim.finki.emt2019.ordermanagement.domain.commands;

/**
 * @author Riste Stojanov
 */
public class ExpireCartCommand {

    public String cartId;

    public ExpireCartCommand(String cartId) {
        this.cartId = cartId;
    }
}
