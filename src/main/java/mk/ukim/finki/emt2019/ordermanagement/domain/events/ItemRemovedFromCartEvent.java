package mk.ukim.finki.emt2019.ordermanagement.domain.events;

import mk.ukim.finki.emt2019.ordermanagement.model.OrderItem;

/**
 * @author Riste Stojanov
 */
public class ItemRemovedFromCartEvent {

    public String cartId;

    public OrderItem item;
}
