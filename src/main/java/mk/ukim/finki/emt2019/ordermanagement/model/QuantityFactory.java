package mk.ukim.finki.emt2019.ordermanagement.model;

/**
 * @author Riste Stojanov
 */
public class QuantityFactory {

    public static Quantity of(double quantity, Unit unit) {
        return new Quantity(quantity, unit);
    }

    public static Quantity parse(String serialized) {
        String[] parts = serialized.split("\\|");
        Double quantity = Double.parseDouble(parts[0]);
        Unit unit = Unit.valueOf(parts[1]);
        return new Quantity(quantity, unit);
    }
}
