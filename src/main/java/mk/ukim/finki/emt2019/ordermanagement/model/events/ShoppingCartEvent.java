package mk.ukim.finki.emt2019.ordermanagement.model.events;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import org.springframework.context.ApplicationEvent;

import java.time.LocalDateTime;

/**
 * @author Riste Stojanov
 */
public class ShoppingCartEvent extends ApplicationEvent {

    private final LocalDateTime when;

    public ShoppingCartEvent(ShoppingCart source, LocalDateTime when) {
        super(source);
        this.when = when;
    }

    public ShoppingCartEvent(ShoppingCart source) {
        super(source);
        this.when = LocalDateTime.now();
    }

    public ShoppingCart getCart() {
        return (ShoppingCart) getSource();
    }

    public LocalDateTime getWhen() {
        return when;
    }
}
