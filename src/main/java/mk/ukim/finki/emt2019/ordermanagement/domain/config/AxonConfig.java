package mk.ukim.finki.emt2019.ordermanagement.domain.config;

import org.axonframework.eventsourcing.EventCountSnapshotTriggerDefinition;
import org.axonframework.eventsourcing.Snapshotter;
import org.axonframework.spring.eventsourcing.SpringAggregateSnapshotterFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Riste Stojanov
 */
@Configuration
public class AxonConfig {

//    @Value("${axon.snapshot.trigger.treshold.finance}")
    private Integer snapshotTriggerTreshold = 100;

    @Bean(name = "eventCountSnapshotTriggerDefinition")
    public EventCountSnapshotTriggerDefinition eventCountSnapshotTriggerDefinition(Snapshotter snapshotter) {
        return new EventCountSnapshotTriggerDefinition(snapshotter, snapshotTriggerTreshold);
    }

    @Bean
    public SpringAggregateSnapshotterFactoryBean snapshotterFactoryBean() {
        return new SpringAggregateSnapshotterFactoryBean();
    }
}


