package mk.ukim.finki.emt2019.ordermanagement.model;

import mk.ukim.finki.emt2019.ordermanagement.model.exceptions.NoProductInCartException;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Riste Stojanov
 */
@Entity
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long cartId;


    public LocalDateTime expiryTime;

    @ManyToOne
    public Account account;

    public Double totalPrice = 0D;

    public Integer itemsCount = 0;

    public Boolean overfilled = false;

    /*
        Information about orphanRemoval: https://www.objectdb.com/java/jpa/persistence/delete#Orphan_Removal_
     */
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cart_id")
    public List<OrderItem> items = new ArrayList<>();

    public static ShoppingCart createWithExpiryInHours(Long expiresAfterHours) {
        ShoppingCart cart = new ShoppingCart();
        cart.expiryTime = LocalDateTime.now().plusHours(expiresAfterHours);
        return cart;
    }

    public ShoppingCart linkToAccount(Account account) {
        this.account = account;
        return this;
    }

    public ShoppingCart addItem(OrderItem item) {
        OrderItem existing = this.items.stream()
                .filter(i -> i.product.productId.equals(item.product.productId))
                .findFirst()
                .orElse(null);
        if (existing != null) {
            existing.quantity = existing.quantity.add(item.quantity);
            // we should update the price here, since the user is probably seeing the new price
            existing.price = item.price;
        } else {
            this.items.add(item);
        }
        this.totalPrice += item.quantity.quantity * item.product.price;
        this.itemsCount=this.items.size();
        return this;
    }

    public ShoppingCart changeItemQuantity(Long productId, Quantity newQuantity) {
        OrderItem existing = this.items.stream()
                .filter(i -> i.product.productId.equals(productId))
                .findFirst()
                .orElseThrow(NoProductInCartException::new);
        this.totalPrice -= existing.quantity.quantity * existing.product.price;
        existing.quantity = newQuantity;
        this.totalPrice += existing.quantity.quantity * existing.product.price;
        return this;
    }

    public ShoppingCart removeItem(Long productId) {
        OrderItem item = this.items.stream()
                .filter(i -> i.product.productId.equals(productId))
                .findFirst()
                .orElseThrow(NoProductInCartException::new);

        this.items.remove(item);
        this.totalPrice -= item.quantity.quantity * item.product.price;
        this.itemsCount=this.items.size();
        return this;
    }

    public ShoppingCart markAsOverfilled() {
        this.overfilled = true;
        return this;
    }

    public boolean hasLinkedAccount() {
        return this.account != null;
    }


    public boolean isExpired() {
        return LocalDateTime.now().isAfter(this.expiryTime);
    }
}
