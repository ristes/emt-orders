package mk.ukim.finki.emt2019.ordermanagement.config.security.oauth;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OAuthProvider {
}
