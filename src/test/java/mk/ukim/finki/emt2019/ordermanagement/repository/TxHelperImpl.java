package mk.ukim.finki.emt2019.ordermanagement.repository;

import mk.ukim.finki.emt2019.ordermanagement.model.ShoppingCart;
import mk.ukim.finki.emt2019.ordermanagement.repository.jpa.ShoppingCartRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnitUtil;
import java.util.Optional;

/**
 * @author Riste Stojanov
 */
@Service
//@Profile("test")
@Profile({"testmem", "test"})
public class TxHelperImpl implements TxHelper {

    private final ShoppingCartRepository cartRepository;

    public TxHelperImpl(ShoppingCartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    private PersistenceUnitUtil unitUtil;

    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)
    public ShoppingCart readCartInTx(Long cartId) {
        unitUtil=em.getEntityManagerFactory().getPersistenceUnitUtil();
        System.out.println("\nQueries issued by find: ");
        Optional<ShoppingCart> cartOptional = this.cartRepository.findById(cartId);
        ShoppingCart cart = null;
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
            System.out.println(String.format("Cart loaded: %b\nAccount loaded: %b\nItems loaded: %b",
                    unitUtil.isLoaded(cart), unitUtil.isLoaded(cart, "account"), unitUtil.isLoaded(cart, "items")));
            System.out.println(String.format("Number of items loaded: %d\n", cart.items.size()));
        }
        return cart;
    }


    @Transactional(readOnly = true)
    public ShoppingCart fetchCartInTx(Long cartId) {
        unitUtil=em.getEntityManagerFactory().getPersistenceUnitUtil();
        System.out.println("\nQueries issued by fetch:");
        Optional<ShoppingCart> cartOptional = this.cartRepository.fetchById(cartId);
        ShoppingCart cart = null;
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
            System.out.println(String.format("Cart loaded: %b\nAccount loaded: %b\nItems loaded: %b",
                    unitUtil.isLoaded(cart), unitUtil.isLoaded(cart, "account"), unitUtil.isLoaded(cart, "items")));
            System.out.println(String.format("Number of items loaded: %d\n", cart.items.size()));
        }
        return cart;
    }

    @Transactional(readOnly = true)
    public ShoppingCart fetchWithAttributesCartInTx(Long cartId) {
        unitUtil=em.getEntityManagerFactory().getPersistenceUnitUtil();
        System.out.println("\nQueries issued by fetch:");
        Optional<ShoppingCart> cartOptional = this.cartRepository.findWithAccountAndItemsByCartId(cartId);
        ShoppingCart cart = null;
        if (cartOptional.isPresent()) {
            cart = cartOptional.get();
            System.out.println(String.format("Cart loaded: %b\nAccount loaded: %b\nItems loaded: %b",
                    unitUtil.isLoaded(cart), unitUtil.isLoaded(cart, "account"), unitUtil.isLoaded(cart, "items")));
            System.out.println(String.format("Number of items loaded: %d\n", cart.items.size()));
        }

        return cart;
    }
}
